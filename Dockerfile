FROM node:alpine AS build-stage
COPY frontend /frontend
WORKDIR /frontend
RUN ["npm", "install"]
RUN ["./node_modules/.bin/rollup", "-c", "./rollup.config.js"]

FROM 84codes/crystal:latest
COPY backend /backend
COPY --from=build-stage /frontend/dist /backend/public
WORKDIR /backend
RUN ["shards", "install"]
RUN ["/usr/bin/crystal", "build", "src/app.cr", "--release"]
ENTRYPOINT ["./app"]
CMD ["-p", "8000"]