# An archive for bad ideas

I have always been thinking about what could worth anyone's time to visit a website about me. Good ideas, finished ideas, impressive projects, sure. That's fine. But what would be more fascinating are all the ideas that I have discarded during all my creative process. One bad idea from me could serves you very well in a different context. Well of course, I would also put my projects up here, but the first thing you would see are all bad ideas.

# A Fake Business

What a bummer, why can’t I find a good idea? No worries, we have millions of bad ideas, lining up to be a good idea to inspire you. How to contribute? Easy, try to make a good idea of a topic, then anything on the way that does not work, just send it to us.

A good line for business, huh? I'm joking. But if you have any bad ideas that you want to share with me, feel free to email me at <anthonytsang0226@gmail.com>!

# How to use?

```bash
cd tsangheiyiu.com_v2
docker compose up --build -d
```

Then the project is hosted on localhost:8000.